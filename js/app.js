// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.run(function($rootScope, Storage, BtData) {
    $rootScope.connectionStatus = "Disconnected";
    $rootScope.btData = BtData;
    
    var connectable = false;
    $rootScope.bluetooth = {
        initialize: function() {
            $rootScope.connectButton = "Bluetooth plugin unavailable";
            this.bindEvents();
        },
        bindEvents: function() {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        onDeviceReady: function() {
            $rootScope.connectButton = "Connect";
            connectable = true;
        },
        connect: function() {
            if (connectable) {
                $rootScope.connectButton = "Connecting to " + Storage.getDevice().name + "...";
                $rootScope.connectionStatus = "Connecting...";
                connectable = false;
                bluetoothSerial.connect(Storage.getDevice().mac, $rootScope.bluetooth.onConnect, $rootScope.bluetooth.onDisconnect);
            } else if (typeof bluetoothSerial != 'undefined') {
                bluetoothSerial.isConnected(function(connected) {
                    if (connected) {
                        bluetoothSerial.disconnect();
                    }
                });
            }
        },
        onConnect: function() {
            bluetoothSerial.subscribe("\n", $rootScope.bluetooth.onMessage, $rootScope.bluetooth.subscribeFailed);
            $rootScope.connectButton = "Disconnect";
            $rootScope.connectionStatus = "Connected";
            $rootScope.bluetooth.send("connect");
        },
        onDisconnect: function() {
            alert("Connection failed");
            $rootScope.connectButton = "Connect";
            $rootScope.connectionStatus = "Disconnected";
            connectable = true;
        },
        onMessage: function(data) {
            $rootScope.incommingMessage(data);
        },
        subscribeFailed: function() {
            alert("Subscribe failed");
        },
        send: function(data) {
            console.log(data);
            if (typeof bluetoothSerial != 'undefined') {
                bluetoothSerial.isConnected(bluetoothSerial.write(data + '\n', $rootScope.bluetooth.onSendSuccess, $rootScope.bluetooth.onSendFailed));
            }
        },
        onSendSuccess: function() {
            console.log("Data sent successfully");
        },
        onSendFailed: function() {
            alert("Send failed");
        }
    };

    $rootScope.incommingMessage = function(data) {
        var keyValue = data.split('=');
        BtData.set(keyValue[0], keyValue[1]);
    };
    $rootScope.bluetooth.initialize();
})

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:

    .state('tab.dash', {
        url: '/dash',
        views: {
            'tab-dash': {
                templateUrl: 'templates/tab-dash.html',
                controller: 'DashCtrl'
            }
        }
    })

    .state('tab.statistics', {
        url: '/statistics',
        views: {
            'tab-statistics': {
                templateUrl: 'templates/tab-statistics.html',
                controller: 'StatsCtrl'
            }
        }
    })
    .state('tab.statistics-detail', {
        url: '/statistics/:statId',
        views: {
            'tab-statistics': {
                templateUrl: 'templates/statistics-detail.html',
                controller: 'StatsDetailCtrl'
            }
        }
    })

    .state('tab.locate', {
        url: '/locate',
        views: {
            'tab-locate': {
                templateUrl: 'templates/tab-locate.html',
                controller: 'LocateCtrl'
            }
        }
    })

    .state('tab.settings', {
        url: '/settings',
        views: {
            'tab-settings': {
                templateUrl: 'templates/tab-settings.html',
                controller: 'SettingsCtrl'
            }
        }
    })

    .state('tab.diagnostics', {
        url: '/settings/diagnostics',
        views: {
            'tab-settings': {
                templateUrl: 'templates/diagnostics.html',
                controller: 'DiagnosticsCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/dash');

});