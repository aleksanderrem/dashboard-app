angular.module('starter.controllers', []).controller('DashCtrl', function($scope) {

    $scope.resetTrip = function() {
        $scope.tripDistance = 0;
    }

}).controller('DrivingCtrl', function($rootScope, $scope, $interval) {
    var needle = document.getElementById('needle');
    var speedometer = document.getElementById('speedometer');
    
    $scope.$watch('btData.getKmh()', function() {
        needle.style.webkitTransform = "rotate(" + ($rootScope.btData.getKmh() - 5) * 9 + "deg)";
    });

    var periodicalID = $interval(function() {
        if ($rootScope.btData.getStatus() == 'Drive') {
            // matrix-to-degree conversion from http://css-tricks.com/get-value-of-css-rotation-through-javascript/
            var st = window.getComputedStyle(needle, null);
            var tr = st.getPropertyValue("-webkit-transform") ||
                st.getPropertyValue("-moz-transform") ||
                st.getPropertyValue("-ms-transform") ||
                st.getPropertyValue("-o-transform") ||
                st.getPropertyValue("transform") ||
                "fail...";

            var values = tr.split('(')[1];
            values = values.split(')')[0];
            values = values.split(',');
            var a = values[0];
            var b = values[1];
            var c = values[2];
            var d = values[3];

            // convert from radians to angle, offset by 45 degrees
            var radians = Math.atan2(b, a) + (45 * Math.PI / 180);
            if (radians < 0) {
                radians += (2 * Math.PI);
            }
            var angle = radians * (180 / Math.PI);
            // Convert from angle to max 30 km: 270 / 30 = 9
            $scope.currentSpeed = (angle / 9).toFixed(1);
        }
    }, 20);
    
    $scope.$on('$destroy', function() {
        $interval.cancel(periodicalID);
    });

}).controller('StatsCtrl', function($scope, Stats) {
    $scope.listOfStats = Stats.all();
}).controller('StatsDetailCtrl', function($scope, $stateParams, Stats) {
    $scope.stat = Stats.get($stateParams.statId);
}).controller('SettingsCtrl', function($rootScope, $scope, $ionicPopup) {

    $scope.drivingLights = $rootScope.btData.getDrivingLights();
    $scope.standbyLights = $rootScope.btData.getStandbyLights();
    $scope.speedCap = $rootScope.btData.getSpeedCap();

    $scope.data = {};
    $scope.data.pin = 1234;

    $scope.changePinPopup = function() {

        $ionicPopup.show({
            template: '<input type="number" ng-model="data.pin">',
            title: 'Enter a new bluetooth PIN-code',
            scope: $scope,
            buttons: [{
                text: 'Cancel'
            }, {
                text: '<b>Save</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.pin) {
                        //don't allow the user to close unless he enters wifi password
                        e.preventDefault();
                    } else {
                        return $scope.data.pin;
                    }
                }
            }, ]
        }).then(function(res) {
            if (res) {
                $scope.bluetooth.send("newpin=" + res);
            }
        });
    };
}).controller('DiagnosticsCtrl', function($rootScope, $scope, $ionicPopup) {

    $scope.getFrontBarValue = function() {
        var result = ($rootScope.btData.getFront() - $rootScope.btData.getBack()) / ($rootScope.btData.getFront() + $rootScope.btData.getBack()) * 100;
        if (result > 0) return result;
        return 1;
    };

    $scope.getBackBarValue = function() {
        var result = ($rootScope.btData.getBack() - $rootScope.btData.getFront()) / ($rootScope.btData.getFront() + $rootScope.btData.getBack()) * 100;
        if (result > 0) return result;
        return 1;
    };

    $scope.getBalancePercentage = function() {
        var back = ($rootScope.btData.getBack() - $rootScope.btData.getFront()) / ($rootScope.btData.getFront() + $rootScope.btData.getBack()) * 100;
        if (back > 0) {
            return (back * -1).toFixed(2);
        }
        return (($rootScope.btData.getFront() - $rootScope.btData.getBack()) / ($rootScope.btData.getFront() + $rootScope.btData.getBack()) * 100).toFixed(2);
    };

    $scope.calibrate = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Calibrate weight sensors',
            template: 'Please make sure the board is placed on level ground and that no weight is being applied to it. Calibration will take approximately 5 seconds. Tap OK to start.'
        });
        confirmPopup.then(function(res) {
            if (res) {
                $scope.bluetooth.send('calibrate=1');
            }
        });
    };

}).controller('LocateCtrl', function($rootScope, $scope, $ionicLoading, $ionicPopup, $compile) {
    $scope.locked = false;

    $scope.initialize = function() {
        var mapOptions = {
            center: new google.maps.LatLng(59.893855, 10.7851166),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);
        $scope.map = map;

        $scope.boardMarker = new google.maps.Marker({
            map: $scope.map,
            title: 'My ' + $rootScope.btData.getModel() + ': ' + $rootScope.btData.getStatus() + ' '
        });

        $scope.infoWindow = new google.maps.InfoWindow({
            content: $scope.boardMarker.getTitle(),
            size: new google.maps.Size(50, 50)
        });
        google.maps.event.addListener($scope.boardMarker, 'click', function() {
            $scope.infoWindow.close();
            $scope.infoWindow.open($scope.map, $scope.boardMarker);
        });
    };
    $scope.centerOnBoard = function() {
        if (!$scope.map) {
            return;
        }

        $scope.loading = $ionicLoading.show({
            content: 'Getting current location...',
            showBackdrop: false
        });

        navigator.geolocation.getCurrentPosition(function(pos) {
            $scope.boardMarker.setPosition(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
            $scope.map.panTo(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
            $ionicLoading.hide();
            google.maps.event.trigger($scope.boardMarker, 'click');

        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
    };
    $scope.lockToggle = function() {
        if ($scope.locked) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Unlock Board',
                template: 'Are you sure you want to remotely unlock the board?'
            });
        } else {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Lock Board',
                template: 'Are you sure you want to remotely lock the board?'
            });
        }
        confirmPopup.then(function(res) {
            if (res) {
                $scope.locked = !$scope.locked;
                $scope.bluetooth.send("locked=" + $scope.locked);
            }
        });
    };
    $scope.initialize();
    $scope.centerOnBoard();
});