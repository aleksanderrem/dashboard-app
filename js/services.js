angular.module('starter.services', [])

.factory('Stats', function(BtData) {
    // Might use a resource here that returns a JSON array
    // Some fake testing data
    var listOfStats = [{
        id: 0,
        name: 'Times travelled',
        //value: BtData.getTripCount
        value: 14
    }, {
        id: 1,
        name: 'Total distance traveled',
        //value: BtData.getTotalTrip
        value: 73.8
    }, {
        id: 2,
        name: 'Longest distance traveled',
        //value: BtData.getLongestTrip
        value: 10.3
    }, {
        id: 3,
        name: 'Last distance traveled',
        //value: BtData.getCurrentTrip
        value: 8.3
    }, {
        id: 4,
        name: 'Average distance traveled',
        value: 4.7
        /*value: function() {
            if (BtData.getTripCount() !== 0 && BtData.getTotalTrip() !== 0) {
                return BtData.getTotalTrip() / BtData.getTripCount()
            } return 0;
        }*/
    }, {
        id: 5,
        name: 'Highest speed',
        value: BtData.getHighestSpeed
    }];
    return {
        all: function() {
            return listOfStats;
        },
        get: function(statsId) {
            // Simple index lookup
            return listOfStats[statsId]; 
        }
    }
}).factory('BtData', function() {
    var btData = {
        'model' : '-',
        'status' : 'Disconnected',
        'battery' : '-',
        'kmh' : 0.0,
        'highestSpeed': 0.0,
        'tripCount' : 0,
        'totaltrip' : 0.0,
        'longesttrip' : 0.0,
        'currenttrip' : 0.0,
        'drivinglights' : true,
        'standbylights' : true,
        'speedcap' : 20,
        'front' : 1,
        'back' : 1
    };
    return {
        all: function() {
            return btData;
        },
        getModel: function() {
            return btData.model;
        },
        getStatus: function() {
            return btData.status;
        },
        getBattery: function() {
            return btData.battery;
        },
        getKmh: function() {
            return btData.kmh;
        },
        getHighestSpeed: function() {
            return btData.highestSpeed;
        },
        getTripCount: function() {
            return btData.tripCount;
        },
        getTotalTrip: function() {
            return btData.totaltrip;
        },
        getLongestTrip: function() {
            return btData.longesttrip;
        },
        getCurrentTrip: function() {
            return btData.currenttrip;
        },
        getDrivingLights: function() {
            return btData.drivinglights;
        },
        getStandbyLights: function() {
            return btData.standbylights;
        },
        getSpeedCap: function() {
            return btData.speedcap;
        },
        getFront: function() {
            return btData.front;
        },
        getBack: function() {
            return btData.back;
        },
        set: function(key, value) {
            key = key.toLowerCase();
            if(key == 'model') {
                btData.model = value.trim();
            } else if(key == 'status') {
                btData.status = value.trim();
            } else if(key == 'battery') {
                btData.battery = parseInt(value);
            } else if(key == 'kmh') {
                btData.kmh = parseFloat(value);
            } else if(key == 'totaltrip') {
                btData.totaltrip = parseFloat(value);
            } else if(key == 'longesttrip') {
                btData.longesttrip = parseFloat(value);
            } else if(key == 'currenttrip') {
                btData.currenttrip = parseFloat(value);
            } else if(key == 'drivinglights') {
                btData.drivinglights = value == 1;
            } else if(key == 'drivinglights') {
                btData.standbylights = value == 1;
            } else if(key == 'speedcap') {
                btData.speedcap = parseInt(value);
            } else if(key == 'sensor') {
                value = value.split(',');
                btData.front = parseInt(value[0]);
                btData.back = parseInt(value[1]);
            }
        }
    }
}).factory('Storage', function() {
    var storage = {
        'device' : {
            'name' : 'Powerboard Alpha',
            'mac' : '10:14:06:24:10:89'
        }
    };
    return {
        getDevice: function() {
            return storage.device;
        }
    }
});